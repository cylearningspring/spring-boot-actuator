package io.gitlab.cylearningspring.springbootactuator.actuator.endpoints;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Component
@Endpoint(id = "customEndpoint")
public class CustomEndpoint {
    @ReadOperation
    public List<String> invoke() {
        return Arrays.asList("Kypros", "Elena", "Nicoletta");
    }
}
