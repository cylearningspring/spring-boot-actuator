package io.gitlab.cylearningspring.springbootactuator.actuator.endpoints.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import io.gitlab.cylearningspring.springbootactuator.utils.ChuckNorrisFactGenerator;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Component
public class ChuckNorrisApplicationInformation implements InfoContributor {
    private final ChuckNorrisFactGenerator chuckNorrisFactGenerator;

    @Autowired
    public ChuckNorrisApplicationInformation(ChuckNorrisFactGenerator chuckNorrisFactGenerator) {
        this.chuckNorrisFactGenerator = chuckNorrisFactGenerator;
    }

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("chuckNorrisFact", chuckNorrisFactGenerator.random());
    }
}
