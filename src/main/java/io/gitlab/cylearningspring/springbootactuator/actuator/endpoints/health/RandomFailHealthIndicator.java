package io.gitlab.cylearningspring.springbootactuator.actuator.endpoints.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
//@Component
public class RandomFailHealthIndicator implements HealthIndicator {
    private final Random random = new Random();

    @Override
    public Health health() {
        if (random.nextBoolean()) {
            return Health.down().withDetail("ERR-001", "Random failure").build();
        }

        return Health.up().build();
    }
}
