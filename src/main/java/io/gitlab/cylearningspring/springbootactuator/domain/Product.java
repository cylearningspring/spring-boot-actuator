package io.gitlab.cylearningspring.springbootactuator.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    private Integer version;

    @CreatedDate
    private Date dateCreated;

    @LastModifiedDate
    private Date lastUpdated;

    private String courseName;
    private String courseSubtitle;

    @Column(length = 2000)
    private String courseDescription;

    @ManyToOne
    private Author author;

    private BigDecimal price;

    @ManyToMany
    private List<ProductCategory> categories = new ArrayList<>();

    private String imageUrl;
}
