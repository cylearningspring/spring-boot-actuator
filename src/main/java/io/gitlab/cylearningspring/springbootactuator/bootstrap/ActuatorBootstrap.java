package io.gitlab.cylearningspring.springbootactuator.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.gitlab.cylearningspring.springbootactuator.domain.Author;
import io.gitlab.cylearningspring.springbootactuator.domain.Product;
import io.gitlab.cylearningspring.springbootactuator.domain.ProductCategory;
import io.gitlab.cylearningspring.springbootactuator.repositories.AuthorRepository;
import io.gitlab.cylearningspring.springbootactuator.repositories.ProductCategoryRepository;
import io.gitlab.cylearningspring.springbootactuator.repositories.ProductRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
@Profile("dev")
@Component
public class ActuatorBootstrap implements CommandLineRunner {
    // Using same description for simplicity
    private static final String COURSE_DESCRIPTION = "Why would you want to learn about the Spring Framework? Simple, Spring is the most widely used framework in the enterprise today. Major companies all over the world are hiring programmers who know the Spring Framework.\\n\" +\n" +
        "                    \"\\n\" +\n" +
        "                    \"My Introduction Spring Framework Tutorial is designed to give you an introduction to the Spring Framework. This course is written for beginners. Ideally before taking the course, you should already have a foundation with the Java programming language. You don't need to be an expert in Java, but you should the basics of Object Oriented Programming with Java.\\n\" +\n" +
        "                    \"\\n\" +\n" +
        "                    \"You will learn what Dependency Injection is, and how Spring uses Inversion of Control to leverage Dependency Injection. Next in my course, I will walk you step by step through building your very first Spring Framework application. I'll show you hot to use the Spring Initializer and Spring Boot to jumpstart your Spring Framework project. Ideally, you can follow along and create your own Spring project. I know it can be frustrating to follow along in a course and run into errors. So don't worry, I have the complete source code examples in Git for you to checkout and use.\");\n";

    private final ProductRepository productRepository;
    private final ProductCategoryRepository productCategoryRepository;
    private final AuthorRepository authorRepository;

    @Autowired
    public ActuatorBootstrap(ProductRepository productRepository, ProductCategoryRepository productCategoryRepository, AuthorRepository authorRepository) {
        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.authorRepository = authorRepository;
    }

    /*
     * Create initial data for development.
     * Create one author: John Thomson, using instructor_jt.png as image
     * Create Categories:
     *   1. Spring Introduction
     *   2. Spring Core
     *   3. Spring Boot
     *   4. Thymeleaf
     * Create one product for each of the courses based on the images.
     * Each product should have the course name, author, image and some guess on the categories.
     *
     * Then course subtitle, description and price should be copied from instructor's project
     */
    @Override
    public void run(String... args) throws Exception {
        // Load Author
        log.info("Loading Author(s)");
        final Author jt = createAuthor("John", "Thomson", "instructor_jt.jpg");

        // Load Product Categories
        log.info("Loading Product Categories");
        final ProductCategory springIntroductionCategory = createProductCategory("Spring Introduction");
        final ProductCategory springCoreCategory = createProductCategory("Spring Core");
        final ProductCategory springBootCategory = createProductCategory("Spring Boot");
        final ProductCategory thymeleafCategory = createProductCategory("Thymeleaf");

        // Load Products
        log.info("Loading Products");
        createProduct(
            jt,
            Arrays.asList(springCoreCategory, springBootCategory),
            "Spring Core Advanced",
            "Advanced Spring Core!",
            "SpringCoreAdvancedThumb.png",
            199,
            COURSE_DESCRIPTION
        );

        createProduct(
            jt,
            Arrays.asList(springCoreCategory, springBootCategory),
            "Spring Core DevOps",
            "Deploying Spring in the Enterprise and the cloud!",
            "SpringCoreDevOpsThumb.png",
            199,
            COURSE_DESCRIPTION
        );

        createProduct(
            jt,
            Arrays.asList(springCoreCategory, springBootCategory),
            "Spring Core",
            "Spring Core - mastering Spring!",
            "SpringCoreThumb.png",
            199,
            COURSE_DESCRIPTION
        );

        createProduct(
            jt,
            Arrays.asList(springCoreCategory, springBootCategory),
            "Spring Core Ultimate",
            "Ultimate Bundle of Spring Core!",
            "SpringCoreUltimateThumb.png",
            199,
            COURSE_DESCRIPTION
        );

        createProduct(
            jt,
            Arrays.asList(springIntroductionCategory, springBootCategory),
            "Spring Intro",
            "Start Learning Spring",
            "SpringIntroThumb.png",
            0,
            COURSE_DESCRIPTION
        );

        createProduct(
            jt,
            thymeleafCategory,
            "Thymeleaf",
            "Thymeleaf and Spring",
            "ThymeleafThumb.png",
            199,
            COURSE_DESCRIPTION
        );
    }

    private Author createAuthor(String firstName, String lastName, String image) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setImage(image);
        author.setDateCreated(new Date());
        author.setLastUpdated(new Date());
        return authorRepository.save(author);
    }

    private ProductCategory createProductCategory(String categoryName) {
        ProductCategory category = new ProductCategory();
        category.setDateCreated(new Date());
        category.setLastUpdated(new Date());
        category.setCategory(categoryName);
        return productCategoryRepository.save(category);
    }

    private Product createProduct(Author jt, ProductCategory category, String courseName, String courseSubtitle, String imageUrl, long price, String courseDescription) {
        return createProduct(jt, Collections.singletonList(category), courseName, courseSubtitle, imageUrl, price, courseDescription);
    }

    private Product createProduct(Author jt, List<ProductCategory> categories, String courseName, String courseSubtitle, String imageUrl, long price, String courseDescription) {
        Product product = new Product();
        product.setAuthor(jt);
        product.setCategories(categories);
        product.setCourseName(courseName);
        product.setCourseSubtitle(courseSubtitle);
        product.setImageUrl(imageUrl);
        product.setPrice(BigDecimal.valueOf(price));
        product.setCourseDescription(courseDescription);
        return productRepository.save(product);
    }
}
