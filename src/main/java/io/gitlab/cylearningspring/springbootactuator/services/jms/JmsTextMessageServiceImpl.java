package io.gitlab.cylearningspring.springbootactuator.services.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class JmsTextMessageServiceImpl implements JmsTextMessageService {
    private final Queue textMessageQueue;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public JmsTextMessageServiceImpl(Queue textMessageQueue, JmsTemplate jmsTemplate) {
        this.textMessageQueue = textMessageQueue;
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendTextMessage(String message) {
        jmsTemplate.convertAndSend(textMessageQueue, message);
    }
}
