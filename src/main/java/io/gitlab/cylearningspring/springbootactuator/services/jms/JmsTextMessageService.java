package io.gitlab.cylearningspring.springbootactuator.services.jms;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface JmsTextMessageService {
    void sendTextMessage(String message);
}
