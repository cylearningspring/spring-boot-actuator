package io.gitlab.cylearningspring.springbootactuator.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import io.gitlab.cylearningspring.springbootactuator.domain.Product;
import io.gitlab.cylearningspring.springbootactuator.repositories.ProductRepository;
import io.gitlab.cylearningspring.springbootactuator.services.jms.JmsTextMessageService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final JmsTextMessageService jmsTextMessageService;
    private final Counter counter;
    private Number lastProductRequested;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository,
                              JmsTextMessageService jmsTextMessageService,
                              MeterRegistry meterRegistry) {
        this.productRepository = productRepository;
        this.jmsTextMessageService = jmsTextMessageService;
        this.counter = meterRegistry.counter("io.gitlab.cylearningspring.springbootactuator.services.getproduct");
        Gauge.builder("io.gitlab.cylearningspring.springbootactuator.services.lastproduct.requested", () -> lastProductRequested)
            .register(meterRegistry);
    }

    @Override
    public Product getProduct(Long id) {
        jmsTextMessageService.sendTextMessage("Fetching Product ID " + id);
        counter.increment();
        lastProductRequested = id;

        return productRepository.findById(id).orElse(null);
    }

    @Override
    public List<Product> listProducts() {
        jmsTextMessageService.sendTextMessage("Listing Products");
        return productRepository.findAll();
    }
}
