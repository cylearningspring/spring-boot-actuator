package io.gitlab.cylearningspring.springbootactuator.services.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
@Component
public class JmsTextMessageListener {

    @JmsListener(destination = "${jms.text-message.queue.name:jms-text-message-queue}")
    public void receiveMessage(String message) {
        log.info("### {} ###", message);
    }
}
