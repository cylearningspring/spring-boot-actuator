package io.gitlab.cylearningspring.springbootactuator.services;

import java.util.List;

import io.gitlab.cylearningspring.springbootactuator.domain.Product;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface ProductService {
    Product getProduct(Long id);

    List<Product> listProducts();
}
