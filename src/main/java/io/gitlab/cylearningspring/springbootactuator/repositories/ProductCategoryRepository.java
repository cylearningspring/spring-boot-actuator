package io.gitlab.cylearningspring.springbootactuator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.gitlab.cylearningspring.springbootactuator.domain.ProductCategory;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

}
