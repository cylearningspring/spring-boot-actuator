package io.gitlab.cylearningspring.springbootactuator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import io.gitlab.cylearningspring.springbootactuator.services.ProductService;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Controller
public class IndexController {
    private ProductService productService;

    @Autowired
    public IndexController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping({"/", "/index.html"})
    public String getIndex(Model model) {
        model.addAttribute("products", productService.listProducts());

        return "index";
    }
}
