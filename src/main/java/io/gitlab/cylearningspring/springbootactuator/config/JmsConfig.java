package io.gitlab.cylearningspring.springbootactuator.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Configuration
public class JmsConfig {

    public final String jmsTextMessageQueueName;

    public JmsConfig(@Value("${jms.text-message.queue.name:jms-text-message-queue}") String jmsQueueName) {
        jmsTextMessageQueueName = jmsQueueName;
    }

    @Bean
    public Queue jmsQueue() {
        return new ActiveMQQueue(jmsTextMessageQueueName);
    }
}
